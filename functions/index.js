const functions = require("firebase-functions");
const { UserEventServiceClient } = require("@google-cloud/retail").v2;

const options = {
    keyFilename: "./credentials.json",
};

exports.sendHomePageViewEvent = functions.analytics
    .event("Home")
    .onLog(async (event) => {
        if (
            event.user.appInfo.appPlatform === "ANDROID" ||
            event.user.appInfo.appPlatform === "IOS"
        ) {
            if (event.user.deviceInfo.resettableDeviceId !== undefined) {
                const eventType = "home-page-view";
                const visitorId = event.user.deviceInfo.resettableDeviceId;
                let homePageViewObj = {};
                homePageViewObj.eventType = eventType;
                homePageViewObj.visitorId = visitorId;

                const retailClient = new UserEventServiceClient(options);

                const parent =
                    "projects/mamaearth-262312/locations/global/catalogs/default_catalog";

                const userEvent = homePageViewObj;

                const request = {
                    parent,
                    userEvent,
                };

                await retailClient.writeUserEvent(request);
            }
        }
    });

exports.sendAddToCartEvent = functions.analytics
    .event("add_to_cart")
    .onLog(async (event) => {
        if (
            event.user.appInfo.appPlatform === "ANDROID" ||
            event.user.appInfo.appPlatform === "IOS"
        ) {
            if (event.user.deviceInfo.resettableDeviceId !== undefined) {
                if (event.params.item_id !== undefined) {
                    const eventType = "add-to-cart";
                    const visitorId = event.user.deviceInfo.resettableDeviceId;
                    let cartId = null;
                    if (event.params.cart_id !== undefined) {
                        cartId = String(event.params.cart_id);
                    }
                    const productDetails = [
                        {
                            product: {
                                id: String(event.params.item_id),
                            },
                            quantity: { value: 1 },
                        },
                    ];
                    let addToCartObj = {};
                    addToCartObj.eventType = eventType;
                    addToCartObj.visitorId = visitorId;
                    addToCartObj.cartId = cartId;
                    addToCartObj.productDetails = productDetails;

                    const retailClient = new UserEventServiceClient(options);

                    const parent =
                        "projects/mamaearth-262312/locations/global/catalogs/default_catalog";

                    const userEvent = addToCartObj;

                    const request = {
                        parent,
                        userEvent,
                    };

                    await retailClient.writeUserEvent(request);
                }
            }
        }
    });

exports.sendDetailPageViewEvent = functions.analytics
    .event("view_item")
    .onLog(async (event) => {
        if (
            event.user.appInfo.appPlatform === "ANDROID" ||
            event.user.appInfo.appPlatform === "IOS"
        ) {
            if (event.user.deviceInfo.resettableDeviceId) {
                if (event.params.productId !== undefined) {
                    const eventType = "detail-page-view";
                    const visitorId = event.user.deviceInfo.resettableDeviceId;
                    const productDetails = {
                        product: {
                            id: String(event.params.productId),
                        },
                    };
                    let detailPageViewObj = {};
                    detailPageViewObj.eventType = eventType;
                    detailPageViewObj.visitorId = visitorId;
                    detailPageViewObj.productDetails = [productDetails];

                    const retailClient = new UserEventServiceClient(options);

                    const parent =
                        "projects/mamaearth-262312/locations/global/catalogs/default_catalog";

                    const userEvent = detailPageViewObj;

                    const request = {
                        parent,
                        userEvent,
                    };

                    await retailClient.writeUserEvent(request);
                }
            }
        }
    });

exports.sendPurchaseCompleteEvent = functions.analytics
    .event("purchase_event")
    .onLog(async (event) => {
        if (
            event.user.appInfo.appPlatform === "ANDROID" ||
            event.user.appInfo.appPlatform === "IOS"
        ) {
            if (event.user.deviceInfo.resettableDeviceId !== undefined) {
                if (event.params.product_id !== undefined) {
                    const eventType = "purchase-complete";
                    const visitorId = event.user.deviceInfo.resettableDeviceId;
                    let productIds = event.params.product_id;
                    productIds = productIds.trim();
                    if (productIds.endsWith(",")) {
                        productIds = productIds.slice(0, productIds.length - 1);
                    }
                    productIds = productIds.split(",");
                    let productDetails = [];
                    productIds.forEach((productId) => {
                        let productInfo = {};
                        productInfo.product = {
                            id: productId.trim(),
                        };
                        productInfo.quantity = { value: 1 };
                        productDetails.push(productInfo);
                    });
                    let purchaseTransaction = {};
                    purchaseTransaction.revenue = event.params.cartAmount;
                    purchaseTransaction.currencyCode = "INR";
                    let purchaseCompleteObj = {};
                    purchaseCompleteObj.eventType = eventType;
                    purchaseCompleteObj.visitorId = visitorId;
                    purchaseCompleteObj.productDetails = productDetails;
                    purchaseCompleteObj.purchaseTransaction =
                        purchaseTransaction;

                    const retailClient = new UserEventServiceClient(options);

                    const parent =
                        "projects/mamaearth-262312/locations/global/catalogs/default_catalog";

                    const userEvent = purchaseCompleteObj;

                    const request = {
                        parent,
                        userEvent,
                    };

                    await retailClient.writeUserEvent(request);
                }
            }
        }
    });
